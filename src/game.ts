import * as utils from '@dcl/ecs-scene-utils'
import { FishingManager } from "./fishing/fishingManager"
import { loadPlayer, unloadPlayer } from "./fishing/loadPlayer"

const _scene = new Entity('_scene')
engine.addEntity(_scene)



///Render some stuff
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
_scene.addComponentOrReplace(transform)

const entity = new Entity('entity')
engine.addEntity(entity)
entity.setParent(_scene)
const gltfShape = new GLTFShape("assets/c9b17021-765c-4d9a-9966-ce93a9c323d1/FloorBaseGrass_01/FloorBaseGrass_01.glb")
gltfShape.withCollisions = true
gltfShape.isPointerBlocker = true
gltfShape.visible = true
entity.addComponentOrReplace(gltfShape)
const transform2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
entity.addComponentOrReplace(transform2)

const caribbeanShoreWithRocksPatch = new Entity('caribbeanShoreWithRocksPatch')
engine.addEntity(caribbeanShoreWithRocksPatch)
caribbeanShoreWithRocksPatch.setParent(_scene)
const transform3 = new Transform({
  position: new Vector3(16, 0, 7.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
caribbeanShoreWithRocksPatch.addComponentOrReplace(transform3)
const gltfShape2 = new GLTFShape("assets/75f2ea4e-e061-4627-ae43-9460aa106066/WaterPatchCurve_02/WaterPatchCurve_02.glb")
gltfShape2.withCollisions = true
gltfShape2.isPointerBlocker = true
gltfShape2.visible = true
caribbeanShoreWithRocksPatch.addComponentOrReplace(gltfShape2)

const caribbeanShoreWithRocksPatch2 = new Entity('caribbeanShoreWithRocksPatch2')
engine.addEntity(caribbeanShoreWithRocksPatch2)
caribbeanShoreWithRocksPatch2.setParent(_scene)
const transform4 = new Transform({
  position: new Vector3(16, 0, 7.5),
  rotation: new Quaternion(-7.781870092739773e-16, -0.7071068286895752, 8.429368136830817e-8, -0.7071068286895752),
  scale: new Vector3(1.0000007152557373, 1, 1.0000007152557373)
})
caribbeanShoreWithRocksPatch2.addComponentOrReplace(transform4)
caribbeanShoreWithRocksPatch2.addComponentOrReplace(gltfShape2)

const arecaPalm = new Entity('arecaPalm')
engine.addEntity(arecaPalm)
arecaPalm.setParent(_scene)
const transform5 = new Transform({
  position: new Vector3(9.5, 0, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
arecaPalm.addComponentOrReplace(transform5)
const gltfShape3 = new GLTFShape("assets/b0f2c844-e0ba-443d-9f32-e8ad3c555c6c/JunglePlant_09/JunglePlant_09.glb")
gltfShape3.withCollisions = true
gltfShape3.isPointerBlocker = true
gltfShape3.visible = true
arecaPalm.addComponentOrReplace(gltfShape3)

const arecaPalm2 = new Entity('arecaPalm2')
engine.addEntity(arecaPalm2)
arecaPalm2.setParent(_scene)
const transform6 = new Transform({
  position: new Vector3(9.5, 0, 2),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
arecaPalm2.addComponentOrReplace(transform6)
arecaPalm2.addComponentOrReplace(gltfShape3)

const arecaPalm3 = new Entity('arecaPalm3')
engine.addEntity(arecaPalm3)
arecaPalm3.setParent(_scene)
const transform7 = new Transform({
  position: new Vector3(2.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
arecaPalm3.addComponentOrReplace(transform7)
arecaPalm3.addComponentOrReplace(gltfShape3)

const beachRock = new Entity('beachRock')
engine.addEntity(beachRock)
beachRock.setParent(_scene)
const transform8 = new Transform({
  position: new Vector3(6.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(0.24500000476837158, 0.24500000476837158, 0.24500000476837158)
})
beachRock.addComponentOrReplace(transform8)
const gltfShape4 = new GLTFShape("assets/9c71a19d-783a-4603-a953-b974b484eade/RockBig_01/RockBig_01.glb")
gltfShape4.withCollisions = true
gltfShape4.isPointerBlocker = true
gltfShape4.visible = true
beachRock.addComponentOrReplace(gltfShape4)

const beachRock2 = new Entity('beachRock2')
engine.addEntity(beachRock2)
beachRock2.setParent(_scene)
const transform9 = new Transform({
  position: new Vector3(4.5, 0, 13.5),
  rotation: new Quaternion(0, 0, 1, -5.960464477539063e-8),
  scale: new Vector3(-0.5000001788139343, -0.5000001788139343, -0.5)
})
beachRock2.addComponentOrReplace(transform9)
const gltfShape5 = new GLTFShape("assets/4ee780b8-623e-4848-ae35-c934e2e63d5c/RockBig_06/RockBig_06.glb")
gltfShape5.withCollisions = true
gltfShape5.isPointerBlocker = true
gltfShape5.visible = true
beachRock2.addComponentOrReplace(gltfShape5)

const bigStoneTile = new Entity('bigStoneTile')
engine.addEntity(bigStoneTile)
bigStoneTile.setParent(_scene)
const transform10 = new Transform({
  position: new Vector3(9.000000953674316, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
bigStoneTile.addComponentOrReplace(transform10)
const gltfShape6 = new GLTFShape("assets/08e139ae-3a20-4ceb-a551-99fdbd56216d/StoneRandomBig_01/StoneRandomBig_01.glb")
gltfShape6.withCollisions = true
gltfShape6.isPointerBlocker = true
gltfShape6.visible = true
bigStoneTile.addComponentOrReplace(gltfShape6)

// create trigger area object, setting size and relative position
let triggerBox = new utils.TriggerBoxShape(new Vector3(2,4,2), new Vector3(-1,0,-1))

bigStoneTile.addComponent(
  new utils.TriggerComponent(
    triggerBox,
    {
      onCameraEnter : () => {
        FishingManager.hasGameLoaded = true
        loadPlayer()
      },
      onCameraExit : () => {
        FishingManager.hasGameLoaded = false
        unloadPlayer()
      },
      enableDebug: false,
    }
  ) 
)