import { FishingManager } from "./fishingManager"

// Intermediate variables
let fishingSystem: ISystem
let eButtonAction: () => void
const canvas = new UICanvas()
const message = new UIText(canvas)


message.fontSize = 24
message.width = 120
message.height = 30
message.vAlign = "top"
message.positionX = -80

let hookTime = 0

// To store player elements to load and unload
const playerElements: Entity[] = []

export function loadPlayer(): void {
    hookTime = 0

    /// Show the UI
    message.value = "Press E to Fish"
    canvas.visible = true;

    // E Key
    if(eButtonAction == null){
        eButtonAction = Input.instance.subscribe("BUTTON_UP", ActionButton.PRIMARY, false, e =>  {
            if(!FishingManager.hasGameLoaded){
                return;
            }

            if(!FishingManager.isFishing) {
                FishingManager.isFishing = true
                message.value = "Fishing..."
                let waitTime = Math.floor(Math.random() * 8) + 3;
                hookTime = Date.now() + waitTime * 1000;
            }
            else {
                if(Math.abs(Date.now() - hookTime) < 1000){
                    message.value = "You caught a fish!"
                }
                else {
                    message.value = "Press E to Fish"
                }
        
                FishingManager.isFishing = false
            }
        })
    }


    class FishingChecker {
        update(dt: number) {
            if(FishingManager.isFishing){
                if(Date.now() > hookTime + 1000){
                    message.value = "It got away :("
                }
                else if(Date.now() > hookTime - 1000) {
                    message.value = "You got a bite :o"
                }
        
            }
        }
    }

    fishingSystem = engine.addSystem(new FishingChecker(), 0)
}

export function unloadPlayer() {
    canvas.visible = false;
  
    hookTime = 0
    FishingManager.isFishing = false

    engine.removeSystem(fishingSystem)
}